package com.yazaki.myapplication.ui.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.yazaki.myapplication.R;
import com.yazaki.myapplication.common.Constants;
import com.yazaki.myapplication.common.SharedPreferencesManager;
import com.yazaki.myapplication.data.MyViewModel;
import com.yazaki.myapplication.retrofit.request.firebase.RequestTokenFirebase;
import com.yazaki.myapplication.retrofit.response.profile.ResponseProfile;
import com.yazaki.myapplication.ui.LoginActivity;

public class ProfileFragment extends Fragment {
    private OnProfileFragmentListener listener;
    private MyViewModel viewModel;
    private TextView tvProfileName, tvProfileSAP, tvProfileEmail, tvProfileArea, tvProfilePhone;
    private Button btnSignOff;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this).get(MyViewModel.class);
//        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppThemeNoActionBarLogin);
//        LayoutInflater li = inflater.cloneInContext(contextThemeWrapper);
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        findViews(v);
        clicks();
        return v;
    }

    private void findViews(View v) {
        btnSignOff = v.findViewById(R.id.buttonSignOff);
        tvProfileName = v.findViewById(R.id.textViewProfileName);
        tvProfileSAP = v.findViewById(R.id.textViewProfileSAP);
        tvProfileEmail = v.findViewById(R.id.textViewProfileEmail);
        tvProfileArea = v.findViewById(R.id.textViewProfileArea);
        tvProfilePhone = v.findViewById(R.id.textViewProfilePhone);
    }

    private void clicks() {
        viewModel.getAllDataProfile().observe(getViewLifecycleOwner(), new Observer<ResponseProfile>() {
            @Override
            public void onChanged(ResponseProfile responseProfile) {
                String name = responseProfile.getUsername();
                String auxRango = responseProfile.getRoles().get(0).getAuthority();
                String rango = auxRango.substring(0, 1).toUpperCase() + auxRango.substring(1);
                String email = responseProfile.getEmail();
                String phone = responseProfile.getTelefono();
                String sapid = responseProfile.getSapid();

                SharedPreferencesManager.setSomeStringValue(Constants.PROFILE_USERNAME, name);
                SharedPreferencesManager.setSomeStringValue(Constants.PROFILE_SAP_ID, sapid);
                SharedPreferencesManager.setSomeStringValue(Constants.PROFILE_AREA, rango);
                SharedPreferencesManager.setSomeStringValue(Constants.PROFILE_EMAIL, email);
                SharedPreferencesManager.setSomeStringValue(Constants.PROFILE_PHONE, phone);

                tvProfileName.setText(name);
                tvProfileSAP.setText(sapid);
                tvProfileEmail.setText(email);
                tvProfileArea.setText(rango);
                tvProfilePhone.setText(phone);
            }
        });

        String name = SharedPreferencesManager.getSomeStringValue(Constants.PROFILE_USERNAME);
        String sapid = SharedPreferencesManager.getSomeStringValue(Constants.PROFILE_SAP_ID);
        String rango = SharedPreferencesManager.getSomeStringValue(Constants.PROFILE_AREA);
        String email = SharedPreferencesManager.getSomeStringValue(Constants.PROFILE_EMAIL);
        String phone = SharedPreferencesManager.getSomeStringValue(Constants.PROFILE_PHONE);

        tvProfileName.setText(name);
        tvProfileSAP.setText(sapid);
        tvProfileEmail.setText(email);
        tvProfileArea.setText(rango);
        tvProfilePhone.setText(phone);

        btnSignOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmSignOff();
            }
        });
    }

    private void confirmSignOff() {
        final String tokenFirebase = SharedPreferencesManager.getSomeStringValue(Constants.TOKEN_FIREBASE);
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());

        builder.setTitle("¿Desea cerrar sesión?")
                .setMessage("Si cierra sesíón no podrán llegar las nuevas notificaciones");

        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                RequestTokenFirebase requestTokenFirebase = new RequestTokenFirebase(tokenFirebase);
                viewModel.setRemoveTokenFirebase(requestTokenFirebase);

                SharedPreferencesManager.removeDataValue(Constants.LOGIN_USER_SAP);
                SharedPreferencesManager.removeDataValue(Constants.LOGIN_USER_PASS);
                SharedPreferencesManager.removeDataValue(Constants.LOGIN_USER_TOKEN);

                SharedPreferencesManager.removeDataValue(Constants.PROFILE_USERNAME);
                SharedPreferencesManager.removeDataValue(Constants.PROFILE_SAP_ID);
                SharedPreferencesManager.removeDataValue(Constants.PROFILE_AREA);
                SharedPreferencesManager.removeDataValue(Constants.PROFILE_EMAIL);
                SharedPreferencesManager.removeDataValue(Constants.PROFILE_PHONE);

                SharedPreferencesManager.removeDataValue(Constants.ID_MESSAGE_SELECTED);

                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public interface OnProfileFragmentListener {
        void profile(String profile);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnProfileFragmentListener) {
            listener = (OnProfileFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " OnProfileFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
