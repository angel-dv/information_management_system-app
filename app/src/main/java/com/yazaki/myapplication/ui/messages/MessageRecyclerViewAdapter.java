package com.yazaki.myapplication.ui.messages;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.yazaki.myapplication.R;
import com.yazaki.myapplication.retrofit.response.messages.MessageList;

import java.util.List;

public class MessageRecyclerViewAdapter extends RecyclerView.Adapter<MessageRecyclerViewAdapter.ViewHolder> {

    private OnItemClickListener listener;
    private List<MessageList> messageList;
    private Context ctx;

    public MessageRecyclerViewAdapter(Context context, List<MessageList> messageList) {
        this.ctx = context;
        this.messageList = messageList;
    }

    public void setData(List<MessageList> messageList) {
        this.messageList = messageList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.message = messageList.get(position);
        String date = getFecha(holder.message.getFecha());
//                + " " + getTime(holder.message.getHora());
//        Glide.with(ctx).load("").placeholder(R.drawable.ic_profile).into(holder.ivAvatarSender);
//        holder.ivAvatarSender.setBackgroundResource(R.drawable.ic_profile);
//        TODO: holder.tvSender.setText(holder.message.getUsuario().getUsername());
        holder.tvDate.setText(date);
        holder.tvContent.setText(holder.message.getTitulo());

        if (holder.message.getConf() != null) {
            holder.clItemMessage.setBackgroundResource(R.color.colorMessageReceive);
            holder.tvSender.setTextColor(ctx.getResources().getColor(android.R.color.darker_gray));
            holder.tvDate.setTextColor(ctx.getResources().getColor(android.R.color.darker_gray));
            holder.tvContent.setTextColor(ctx.getResources().getColor(android.R.color.black));
        } else {
            holder.clItemMessage.setBackgroundResource(R.color.colorMessageNoReceive);
            holder.tvSender.setTextColor(ctx.getResources().getColor(android.R.color.black));
            holder.tvDate.setTextColor(ctx.getResources().getColor(android.R.color.black));
            holder.tvContent.setTextColor(ctx.getResources().getColor(android.R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        if (messageList == null)
            return 0;
        else return messageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View v;
        public MessageList message;
        public ConstraintLayout clItemMessage;
        public LinearLayout linearLayout;
        public final TextView tvDate, tvSender, tvContent;
        public ImageView ivAvatarSender;

        public ViewHolder(View view) {
            super(view);
            v = view;
            clItemMessage = v.findViewById(R.id.constraintLayoutItemMessage);
            linearLayout = v.findViewById(R.id.linearLayoutMessage);
            ivAvatarSender = v.findViewById(R.id.imageViewAvatarSender);
            tvSender = v.findViewById(R.id.textViewSender);
            tvDate = v.findViewById(R.id.textViewDatePerson);
            tvContent = v.findViewById(R.id.textViewMessageContentPerson);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClicked(messageList.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(MessageList message);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public String getFecha(String fec) {
        String fecha = "";
        String dia = fec.substring(8), mesNum = fec.substring(5, 7), anio = fec.substring(0, 4), mes = "";
        switch (mesNum) {
            case "01":
                mes = "Enero";
                break;
            case "02":
                mes = "Febrero";
                break;
            case "03":
                mes = "Marzo";
                break;
            case "04":
                mes = "Abril";
                break;
            case "05":
                mes = "Mayo";
                break;
            case "06":
                mes = "Junio";
                break;
            case "07":
                mes = "Julio";
                break;
            case "08":
                mes = "Agosto";
                break;
            case "09":
                mes = "Septiembre";
                break;
            case "10":
                mes = "Octubre";
                break;
            case "11":
                mes = "Noviembre";
                break;
            case "12":
                mes = "Diciembre";
                break;
        }

        fecha = dia + " de " + mes + " de " + anio;

        return fecha;
    }

    public String getTime(String hrs) {
        String time = "";
        String hr = hrs.substring(0, 2);
        if (Integer.parseInt(hr) > 12) {
            time = Integer.parseInt(hr) - 12 + hrs.substring(2, 5) + " p.m.";
        } else {
            time = hr + hrs.substring(2, 5) + " a.m.";
        }
        return time;
    }

}
