package com.yazaki.myapplication.ui.controller;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.yazaki.myapplication.R;
import com.yazaki.myapplication.ui.messages.MessageFragment;
import com.yazaki.myapplication.ui.profile.ProfileFragment;

public class PlaceholderFragment extends Fragment {

    public static Fragment newInstance(int index) {

        Fragment f = null;

        switch (index) {
            case 1:
                f = new MessageFragment();
                break;
            case 2:
                f = new ProfileFragment();
                break;
        }

        return f;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab, container, false);
    }
}