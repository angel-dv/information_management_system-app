package com.yazaki.myapplication.ui.messages;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.yazaki.myapplication.R;
import com.yazaki.myapplication.common.Constants;
import com.yazaki.myapplication.common.Loading;
import com.yazaki.myapplication.common.SharedPreferencesManager;
import com.yazaki.myapplication.data.MyViewModel;
import com.yazaki.myapplication.retrofit.response.messages.Message;

import java.util.Objects;

import okhttp3.ResponseBody;

public class ContentMessage extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tvMessageNamePerson, tvMessageContent, tvToolbarName, tvMessageTitle, tvMessageDate, txtMA1, txtMA2;
    private ImageView ivMessageAvatar;
    private Button btnConfirmationMessage;
    private MyViewModel viewModel;
    private Message myMessage;
    private Integer idMessage = null;
    private int count = 0;
    private String user = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Loading.showProgressDialog(ContentMessage.this);
        viewModel = ViewModelProviders.of(this).get(MyViewModel.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_message);

        Bundle args = getIntent().getExtras();
        if (args != null) {
            idMessage = args.getInt(Constants.ID_MESSAGE_TO_CONTENT_MESSAGE);
            viewModel.setOneMessage(idMessage);
        }

        if (idMessage == null) {
            idMessage = SharedPreferencesManager.getSomeIntValue(Constants.ID_MESSAGE_SELECTED);
            viewOneMessageFirebase(idMessage);
        }


        finds();
        user = SharedPreferencesManager.getSomeStringValue(Constants.LOGIN_USER_SAP);
        String us = "Usuario " + user;
        txtMA1.setText(us);
        txtMA2.setText(us);
        if(checkConnection())
            getMessage(idMessage);
        else{
            Toast.makeText(getApplicationContext(),"No hay conexión a internet",Toast.LENGTH_SHORT).show();
            Loading.removeProgressDialog();
        }
    }

    private void finds() {
        toolbar = findViewById(R.id.toolbarMessages);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvToolbarName = findViewById(R.id.textViewToolbarTitleName);
        tvMessageTitle = findViewById(R.id.textViewMessageTitle);
        ivMessageAvatar = findViewById(R.id.imageViewMessageAvatar);
        tvMessageNamePerson = findViewById(R.id.textViewMessageNamePerson);
        tvMessageDate = findViewById(R.id.textViewMessageDate);
        tvMessageContent = findViewById(R.id.textViewMessageContent);
        btnConfirmationMessage = findViewById(R.id.buttonConfirmationMessage);
        txtMA1 = findViewById(R.id.txtMA1);
        txtMA2 = findViewById(R.id.txtMA2);
    }

    private void getMessage(final int idMessage) {
        ivMessageAvatar.setBackgroundResource(R.drawable.ic_profile);
        viewOneMessage();

        btnConfirmationMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 0) {
                    count = 1;
                    viewModel.setConfirmationMessage(idMessage);
                    viewModel.getConfirmMessage().observe(ContentMessage.this, new Observer<ResponseBody>() {
                        @Override
                        public void onChanged(ResponseBody responseBody) {
                            Log.d("TAG", responseBody.toString());
                        }
                    });

                    Handler h = new Handler();
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            count = 0;
                        }
                    };
                    h.postDelayed(r, 2000);
                }
            }
        });
    }

    private void viewOneMessage() {
        viewModel.getOneMessage().observe(this, new Observer<Message>() {
            @Override
            public void onChanged(Message message) {
                myMessage = message;
                String autor = "De: " + myMessage.getUsuario();
                String date = getFecha(myMessage.getFecha());

                tvToolbarName.setText(myMessage.getUsuario());
                tvMessageTitle.setText(myMessage.getTitulo());
                tvMessageNamePerson.setText(autor);
                tvMessageDate.setText(date);
                tvMessageContent.setText(myMessage.getContenido());

                if (myMessage.getConf() != null) {
                    btnConfirmationMessage.setEnabled(false);
                    btnConfirmationMessage.setBackgroundResource(R.drawable.bg_button_confirm_disable);
                    btnConfirmationMessage.setText("Confirmado");
                }
                Loading.removeProgressDialog();
            }
        });
    }

    private boolean checkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private void viewOneMessageFirebase(int idMessage) {
        if (idMessage != -1) {
            viewModel.setOneMessage(idMessage);
            viewModel.getOneMessageFirebase().observe(this, new Observer<Message>() {
                @Override
                public void onChanged(Message message) {
                    Log.d("TAG", message.toString());
                }
            });
        }
    }

    public String getFecha(String fec) {
        String fecha = "";
        String dia = fec.substring(8), mesNum = fec.substring(5, 7), anio = fec.substring(0, 4), mes = "";
        switch (mesNum) {
            case "01":
                mes = "Enero";
                break;
            case "02":
                mes = "Febrero";
                break;
            case "03":
                mes = "Marzo";
                break;
            case "04":
                mes = "Abril";
                break;
            case "05":
                mes = "Mayo";
                break;
            case "06":
                mes = "Junio";
                break;
            case "07":
                mes = "Julio";
                break;
            case "08":
                mes = "Agosto";
                break;
            case "09":
                mes = "Septiembre";
                break;
            case "10":
                mes = "Octubre";
                break;
            case "11":
                mes = "Noviembre";
                break;
            case "12":
                mes = "Diciembre";
                break;
        }

        fecha = dia + " de " + mes + " de " + anio;

        return fecha;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}