package com.yazaki.myapplication.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.yazaki.myapplication.R;
import com.yazaki.myapplication.TabsActivity;
import com.yazaki.myapplication.common.Constants;
import com.yazaki.myapplication.common.Loading;
import com.yazaki.myapplication.common.SharedPreferencesManager;
import com.yazaki.myapplication.retrofit.ApiClient;
import com.yazaki.myapplication.retrofit.ApiService;
import com.yazaki.myapplication.retrofit.request.login.RequestLogin;
import com.yazaki.myapplication.retrofit.response.login.ResponseLogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG = LoginActivity.class.getSimpleName();
    private ApiClient apiClient;
    private ApiService apiService;
    private ImageView ivAvatar;
    private TextView tvForgotPass;
    private TextInputEditText etEmail, etPass;
    private Button btnLogin, logTokenButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        findViews();
        clicks();
    }

    private void init() {
        apiClient = ApiClient.getInstance();
        apiService = apiClient.getApiService();
    }

    private void findViews() {
        ivAvatar = findViewById(R.id.imageViewAvatar);
        tvForgotPass = findViewById(R.id.textViewForgotPassword);
        etEmail = findViewById(R.id.textInputEditTextEmail);
        etPass = findViewById(R.id.textInputEditTextPassword);
        btnLogin = findViewById(R.id.buttonLogin);
        //Botón para generar token de firebase
        logTokenButton = findViewById(R.id.logTokenButton);
    }

    private void clicks() {
        ivAvatar.setImageDrawable(getResources().getDrawable(R.drawable.design_logo_yazaki));

        tvForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "En producción!", Toast.LENGTH_SHORT).show();
            }
        });

        etPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Loading.showProgressDialog(LoginActivity.this);
                    validPerson();
                    handled = true;
                }
                return handled;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Loading.showProgressDialog(LoginActivity.this);
                validPerson();
            }
        });

        //Método para obtener el token de Messaging para probar notificaciones
        //SOLO PARA PRUEBAS
        logTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get token
                // [START retrieve_current_token]
                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            private static final String TAG = "Firebase Token";

                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {
                                    Log.w(TAG, "getInstanceId failed", task.getException());
                                    return;
                                }
                                // Obtener nuevo Instance ID token
                                String token = task.getResult().getToken();
                                // Log y toast
                                String msg = token;
                                Log.d(TAG, msg);
                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        });
                // [END retrieve_current_token]
            }
        });
    }

    private void validPerson() {
        String sap = etEmail.getText().toString();
        String pass = etPass.getText().toString();

        if (!TextUtils.isEmpty(sap) && !TextUtils.isEmpty(pass)) {
            if (sap.length() == 8 && isValidPassword(pass)) {
                goToHome(sap, pass);
            } else {
                Loading.removeProgressDialog();
                Toast.makeText(this, "Requiere un SAP valido y una contraseña mayor a 4 caracteres", Toast.LENGTH_LONG).show();
            }
        } else {
            Loading.removeProgressDialog();
            Toast.makeText(LoginActivity.this, "Por favor, rellene todos los campos", Toast.LENGTH_SHORT).show();
        }

    }

    private void goToHome(final String sap, final String pass) {
        RequestLogin requestLogin = new RequestLogin(sap, pass);

        Call<ResponseLogin> call = apiService.doLogin(requestLogin);
        call.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if (response.isSuccessful()) {
                    SharedPreferencesManager.setSomeStringValue(Constants.LOGIN_USER_SAP, sap);
                    SharedPreferencesManager.setSomeStringValue(Constants.LOGIN_USER_PASS, pass);
                    SharedPreferencesManager.setSomeStringValue(Constants.LOGIN_USER_TOKEN, response.body().getToken());

                    Toast.makeText(LoginActivity.this, response.body().getMensaje(), Toast.LENGTH_SHORT).show();
                    goToMessage();
                } else {
                    Toast.makeText(LoginActivity.this, "Algo fue mal, revise sus datos de acceso", Toast.LENGTH_SHORT).show();
                    Loading.removeProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Problemas de conexión. Inténtelo de nuevo", Toast.LENGTH_SHORT).show();
                Loading.removeProgressDialog();
                Log.d(TAG, t.getMessage());
            }
        });

    }

    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValidPassword(String password) {
        return password.length() >= 5;
    }

    private void goToMessage() {
        Intent i = new Intent(LoginActivity.this, TabsActivity.class);
        startActivity(i);
        finish();
        Loading.removeProgressDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            String sap = SharedPreferencesManager.getSomeStringValue(Constants.LOGIN_USER_SAP);
            String pass = SharedPreferencesManager.getSomeStringValue(Constants.LOGIN_USER_PASS);

            if (!sap.isEmpty() && !pass.isEmpty()) {
                goToMessage();
            }

        } catch (Exception e) {
        }
    }
}
