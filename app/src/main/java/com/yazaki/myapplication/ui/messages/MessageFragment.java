package com.yazaki.myapplication.ui.messages;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yazaki.myapplication.R;
import com.yazaki.myapplication.common.Constants;
import com.yazaki.myapplication.common.MyApp;
import com.yazaki.myapplication.data.MyViewModel;
import com.yazaki.myapplication.retrofit.response.messages.MessageList;

import java.util.ArrayList;
import java.util.List;

public class MessageFragment extends Fragment {
    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private RecyclerView recyclerView;
    private MessageRecyclerViewAdapter adapter;
    private List<MessageList> personList;
    private OnMessageFragmentListener listener;
    private MyViewModel viewModel;
    private LocalBroadcastManager broadcaster;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String value = intent.getExtras().getString(Constants.ID_MESSAGE_REFRESH);
            if (!value.isEmpty()) {
                viewRefreshMessage();
            }
        }
    };

    public MessageFragment() {
    }

    public static MessageFragment newInstance(int columnCount) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this).get(MyViewModel.class);
        final View v = inflater.inflate(R.layout.fragment_message_list, container, false);
        broadcaster = LocalBroadcastManager.getInstance(getActivity());
        findViews(v);
        personMessage();
        return v;
    }

    private void findViews(View v) {
        recyclerView = v.findViewById(R.id.list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void personMessage() {
        personList = new ArrayList<>();
        adapter = new MessageRecyclerViewAdapter(getContext(), personList);
        recyclerView.setAdapter(adapter);

        viewModel.getAllMessages().observe(getViewLifecycleOwner(), new Observer<List<MessageList>>() {
            @Override
            public void onChanged(List<MessageList> messages) {
                personList = messages;
                adapter.setData(personList);

                adapter.setOnItemClickListener(new MessageRecyclerViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClicked(MessageList message) {
                        int idMessage = message.getId();
                        listener.setIdMessage(idMessage);
                    }
                });

                if (messages.size() == 0) {
                    Toast.makeText(MyApp.getContext(), "No hay mensajes", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public interface OnMessageFragmentListener {
        void setIdMessage(int idMessage);
    }

    private void viewRefreshMessage() {
        if(checkConnection()){
            viewModel.getRefreshMessage().observe(getActivity(), new Observer<List<MessageList>>() {
                @Override
                public void onChanged(@Nullable List<MessageList> messageLists) {
                    personList = messageLists;
                    adapter.setData(personList);
                    viewModel.getRefreshMessage().removeObserver(this);
                }
            });
        } else {
            Toast.makeText(getContext(),"No hay conexión a internet",Toast.LENGTH_SHORT).show();
        }

    }

    private boolean checkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnMessageFragmentListener) {
            listener = (OnMessageFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " OnMessageFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        viewRefreshMessage();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver((mMessageReceiver), new IntentFilter(Constants.MY_DATA_REFRESH));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }

}
