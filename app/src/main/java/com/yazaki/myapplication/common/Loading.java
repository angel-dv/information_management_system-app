package com.yazaki.myapplication.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.yazaki.myapplication.R;

public class Loading {
    private static ProgressDialog pd;

    public static void ProgressDialog(Context context, String title, String msg, boolean isCancel) {
        try {
            if (pd == null) {
                pd = new ProgressDialog(context, R.style.ProgressBar);
                pd.setCancelable(isCancel);
                pd.setMessage(msg);
                pd.setTitle(title);
                pd.show();
            }

            if (!pd.isShowing()) {
                pd.show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showProgressDialog(Context context) {
        ProgressDialog(context, null, "Cargando...", false);
    }

    public static void removeProgressDialog() {
        try {
            if (pd != null) {
                if (pd.isShowing()) {
                    pd.dismiss();
                    pd = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
