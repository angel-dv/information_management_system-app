package com.yazaki.myapplication.common;

public class Constants {
    //public static final String API_BASE_URL = "http://192.168.1.55:8084/api/";
//    public static final String API_BASE_URL = "http://sai.sytes.net:8084/api/";
    public static final String API_BASE_URL = "http://104.129.130.109/sai/api/";

    public static final String LOGIN_USER_SAP = "LOGIN_USER";
    public static final String LOGIN_USER_PASS = "LOGIN_USER_PASS";
    public static final String LOGIN_USER_TOKEN = "LOGIN_USER_TOKEN";

    public static final String PROFILE_USERNAME = "PROFILE_USERNAME";
    public static final String PROFILE_SAP_ID = "PROFILE_SAP_ID";
    public static final String PROFILE_AREA = "PROFILE_AREA";
    public static final String PROFILE_EMAIL = "PROFILE_EMAIL";
    public static final String PROFILE_PHONE = "PROFILE_PHONE";

    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";

    public static final String ID_MESSAGE_SELECTED = "ID_MESSAGE_SELECTED";

    public static final String ID_MESSAGE_TO_CONTENT_MESSAGE = "ID_MESSAGE_TO_CONTENT_MESSAGE";
    public static final String TOKEN_FIREBASE = "TOKEN_FIREBASE";

    public static final String MY_DATA_REFRESH = "MY_DATA_REFRESH";
    public static final String ID_MESSAGE_REFRESH = "ID_MESSAGE_REFRESH";
}

