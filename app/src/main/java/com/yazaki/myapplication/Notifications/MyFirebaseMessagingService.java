package com.yazaki.myapplication.Notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.yazaki.myapplication.R;
import com.yazaki.myapplication.common.Constants;
import com.yazaki.myapplication.common.SharedPreferencesManager;
import com.yazaki.myapplication.ui.messages.ContentMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = "TAG";
    private static final String TAG_NOT = "NOT_VALUES";
    private LocalBroadcastManager broadcaster;

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Imprimir datos que llegan en notificación
        Log.i(TAG_NOT, "\n" + "title: " + String.valueOf(remoteMessage.getData().get("title")) + "\n" + "message: " + remoteMessage.getData().get("message") + "\n" + "notType: " + remoteMessage.getData().get("notType") + "\n" + "extraData: " + remoteMessage.getData().get("extraData") + "\n" + "idMessage " + remoteMessage.getData().get("idMessage"));

        Intent i = new Intent(Constants.MY_DATA_REFRESH);

        ///Recibir notificación
        if (remoteMessage.getData().size() > 0) {
            //Mostrar notificación según parametros del servidor
            showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), remoteMessage.getData().get("notType"), remoteMessage.getData().get("extraData"), Integer.parseInt(remoteMessage.getData().get("idMessage")));
            i.putExtra(Constants.ID_MESSAGE_REFRESH, remoteMessage.getData().get("idMessage"));
            broadcaster.sendBroadcast(i);
        }
        if (remoteMessage.getNotification() != null) {
            showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), remoteMessage.getData().get("notType"), remoteMessage.getData().get("extraData"), Integer.parseInt(remoteMessage.getData().get("idMessage")));
            i.putExtra(Constants.ID_MESSAGE_REFRESH, remoteMessage.getData().get("idMessage"));
            broadcaster.sendBroadcast(i);
        }

    }

    //Usar diseño predeterminado para notificación en Android < Jelly bean
    private RemoteViews getCustomDesign(String title, String message) {
        RemoteViews remoteViews = new RemoteViews(getApplicationContext().getPackageName(), R.layout.notification);
        remoteViews.setTextViewText(R.id.title, title);
        remoteViews.setTextViewText(R.id.message, message);
        remoteViews.setImageViewResource(R.id.icon, R.drawable.design_logo_yazaki);
        return remoteViews;
    }

    public void showNotification(String title, String message, String notType, String extra_data, int idMessage) {
        SharedPreferencesManager.setSomeIntValue(Constants.ID_MESSAGE_SELECTED, idMessage);
        Intent intent = new Intent(this, ContentMessage.class);
        String channel_id = "Actualización de grupo ";
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        //Tono de notificación (DEFAULT)
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = null;

        //Crear notificación por default
        if (notType == null) {
            builder = new NotificationCompat.Builder(getApplicationContext(), channel_id)
                    .setSmallIcon(R.drawable.yazaki_isotipo)
                    .setSound(uri)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setOnlyAlertOnce(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setContentIntent(pendingIntent);

        }
        //Crear notificación tipo inbox
        else if (String.valueOf(notType) == "inbox_style") {
            try {
                NotificationCompat.InboxStyle notStyle = new NotificationCompat.InboxStyle();


                builder = new NotificationCompat.Builder(getApplicationContext(), channel_id)
                        .setSmallIcon(R.drawable.yazaki_isotipo)
                        .setSound(uri)
                        .setAutoCancel(true)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setOnlyAlertOnce(true)
                        .setStyle(notStyle)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setContentIntent(pendingIntent);
            } catch (Exception e) {
                e.printStackTrace();
                builder = new NotificationCompat.Builder(getApplicationContext(), channel_id)
                        .setSmallIcon(R.drawable.yazaki_isotipo)
                        .setSound(uri)
                        .setAutoCancel(true)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setOnlyAlertOnce(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setContentIntent(pendingIntent);
            }

        } else {
            builder = new NotificationCompat.Builder(getApplicationContext(), channel_id)
                    .setSmallIcon(R.drawable.yazaki_isotipo)
                    .setSound(uri)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setOnlyAlertOnce(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setContentIntent(pendingIntent);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && notType == null) {
            builder = builder.setContent(getCustomDesign(title, message));
        } else {
            builder = builder.setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.yazaki_isotipo);
        }

        //Atributos de la notificación, canal de notificación e importancia
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channel_id, "Actualización de grupo", NotificationManager.IMPORTANCE_HIGH);
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            notificationChannel.setSound(uri, audioAttributes);
            notificationChannel.enableVibration(true);
            notificationChannel.setLockscreenVisibility(0);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }

        assert notificationManager != null;
        notificationManager.notify(1125, builder.build());
    }
}

//FirebaseMessaging.getInstance().subscribeToTopic("NombreDelGrupo");

