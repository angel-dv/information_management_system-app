package com.yazaki.myapplication.data;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.yazaki.myapplication.common.Constants;
import com.yazaki.myapplication.common.MyApp;
import com.yazaki.myapplication.common.SharedPreferencesManager;
import com.yazaki.myapplication.retrofit.AuthClient;
import com.yazaki.myapplication.retrofit.AuthService;
import com.yazaki.myapplication.retrofit.request.firebase.RequestTokenFirebase;
import com.yazaki.myapplication.retrofit.request.token.RequestToken;
import com.yazaki.myapplication.retrofit.response.messages.Message;
import com.yazaki.myapplication.retrofit.response.messages.MessageList;
import com.yazaki.myapplication.retrofit.response.profile.ResponseProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
    public static final String TAG = Repository.class.getSimpleName();
    private AuthService authService;
    private MutableLiveData<ResponseProfile> allDataProfile;
    private MutableLiveData<List<MessageList>> allMessages;
    private MutableLiveData<ResponseBody> confirmatedMessage;
    private MutableLiveData<Message> message;
    private MutableLiveData<ResponseBody> tokenFirebase;

    Repository() {
        AuthClient authClient = AuthClient.getInstance();
        authService = authClient.getAuthService();
        allDataProfile = getAllDataProfile();
        allMessages = getAllMessages();
        confirmatedMessage = getConfirmateMessage();
        message = getOneMessage();
        tokenFirebase = getTokenFirebase();
    }

    public MutableLiveData<ResponseProfile> getAllDataProfile() {
        if (allDataProfile == null) {
            allDataProfile = new MutableLiveData<>();
        }

        Call<ResponseProfile> call = authService.doProfile();
        call.enqueue(new Callback<ResponseProfile>() {
            @Override
            public void onResponse(@NonNull Call<ResponseProfile> call, @NonNull Response<ResponseProfile> response) {
                if (response.isSuccessful()) {
                    allDataProfile.setValue(response.body());
                } else {
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseProfile> call, @NonNull Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error en la conexión", Toast.LENGTH_SHORT).show();

            }
        });

        return allDataProfile;
    }

    public MutableLiveData<List<MessageList>> getAllMessages() {
        if (allMessages == null) {
            allMessages = new MutableLiveData<>();
        }

        Call<List<MessageList>> call = authService.getAllMessage();
        call.enqueue(new Callback<List<MessageList>>() {
            @Override
            public void onResponse(@NonNull Call<List<MessageList>> call, @NonNull Response<List<MessageList>> response) {
                if (response.isSuccessful()) {
                    allMessages.setValue(response.body());
                } else {
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<MessageList>> call, @NonNull Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error en la conexión", Toast.LENGTH_SHORT).show();
            }
        });

        return allMessages;
    }

    public MutableLiveData<ResponseBody> getConfirmateMessage() {
        if (confirmatedMessage == null) {
            confirmatedMessage = new MutableLiveData<>();
        }

        return confirmatedMessage;
    }

    public MutableLiveData<Message> getOneMessage() {
        if (message == null) {
            message = new MutableLiveData<>();
        }

        return message;
    }

    public MutableLiveData<ResponseBody> getTokenFirebase() {
        if (tokenFirebase == null) {
            tokenFirebase = new MutableLiveData<>();
        }
        return tokenFirebase;
    }

    public void confirmationMessage(final int id) {
        Call<ResponseBody> call = authService.setConfirmationMessage(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        confirmatedMessage.setValue(response.body());

                        try {
                            Gson gson = new Gson();
                            String value = response.body().string();

                            MessageList messageList = gson.fromJson(value, MessageList.class);
                            Log.d(TAG, messageList.toString());

                            for (int i = 0; i < allMessages.getValue().size(); i++) {
                                if (id == allMessages.getValue().get(i).getId() && id == messageList.getId()) {
                                    allMessages.setValue(Collections.singletonList(new MessageList(messageList, id)));
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Toast.makeText(MyApp.getContext(), "Mensaje confirmado", Toast.LENGTH_SHORT).show();
                    } else if (response.code() == 202) {
                        confirmatedMessage.setValue(response.body());
                        try {
                            String value = response.body().string();
                            JSONObject jsonObject = new JSONObject(value);
                            String message = jsonObject.getString("message");

                            Toast.makeText(MyApp.getContext(), message, Toast.LENGTH_SHORT).show();
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Log.d(TAG, response.toString());
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error en la conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setOneMessage(int id) {
        Call<Message> call = authService.getOneMessage(id);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(@NonNull Call<Message> call, @NonNull Response<Message> response) {
                if (response.isSuccessful()) {
                    message.setValue(response.body());
                } else {
                    Toast.makeText(MyApp.getContext(), "Error al abrir mensaje, por favor intente de nuevo", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Message> call, @NonNull Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error en la conexión", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void setTokenFirebase(RequestToken requestToken) {
        Call<ResponseBody> call = authService.getAccessTokenFirebase(requestToken);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.d(TAG, response.toString());
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        tokenFirebase.setValue(response.body());
                    } else if (response.code() == 202) {
                        tokenFirebase.setValue(response.body());
                    }

                } else {
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal en Token-Firebase", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error en la conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setRemoveTokenFirebase(RequestTokenFirebase requestTokenFirebase) {
        Call<ResponseBody> call = authService.getRemoveTokenFirebase(requestTokenFirebase);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String value = response.body().string();

                        JSONObject jsonObject = new JSONObject(value);
                        String message = jsonObject.getString("message");

                        Toast.makeText(MyApp.getContext(), message, Toast.LENGTH_SHORT).show();
                        SharedPreferencesManager.removeDataValue(Constants.TOKEN_FIREBASE);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(MyApp.getContext(), "A habiado un error en la solicitud", Toast.LENGTH_SHORT).show();
                    SharedPreferencesManager.removeDataValue(Constants.TOKEN_FIREBASE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
