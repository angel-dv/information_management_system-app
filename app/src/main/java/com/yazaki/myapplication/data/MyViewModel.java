package com.yazaki.myapplication.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.yazaki.myapplication.retrofit.request.firebase.RequestTokenFirebase;
import com.yazaki.myapplication.retrofit.request.token.RequestToken;
import com.yazaki.myapplication.retrofit.response.messages.Message;
import com.yazaki.myapplication.retrofit.response.messages.MessageList;
import com.yazaki.myapplication.retrofit.response.profile.ResponseProfile;

import java.util.List;

import okhttp3.ResponseBody;

public class MyViewModel extends AndroidViewModel {
    private Repository repository;
    private LiveData<ResponseProfile> allDataProfile;
    private LiveData<List<MessageList>> allMessages;
    private LiveData<ResponseBody> confirmationMessage;
    private LiveData<Message> message;
    private LiveData<ResponseBody> tokenFirebase;

    public MyViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository();
        allDataProfile = repository.getAllDataProfile();
        allMessages = repository.getAllMessages();
        confirmationMessage = repository.getConfirmateMessage();
        message = repository.getOneMessage();
        tokenFirebase = repository.getTokenFirebase();
    }

    public LiveData<ResponseProfile> getAllDataProfile(){
        return allDataProfile;
    }

    public LiveData<List<MessageList>> getAllMessages(){
        return allMessages;
    }

    public LiveData<ResponseBody> getConfirmMessage() {
        repository.getConfirmateMessage();
        return confirmationMessage;
    }

    public LiveData<Message> getOneMessage() {
        return message;
    }

    public LiveData<ResponseBody> getAllDataTokenFirebase() {
        return tokenFirebase;
    }

    public LiveData<List<MessageList>> getRefreshMessage() {
        allMessages = repository.getAllMessages();
        return allMessages;
    }

    public LiveData<Message> getOneMessageFirebase() {
        message = repository.getOneMessage();
        return message;
    }

    public void setConfirmationMessage(int id){
        repository.confirmationMessage(id);
    }

    public void setOneMessage(int id){
        repository.setOneMessage(id);
    }

    public void setTokenFirebase(RequestToken requestToken) {
        repository.setTokenFirebase(requestToken);
    }

    public void setRemoveTokenFirebase(RequestTokenFirebase requestTokenFirebase) {
        repository.setRemoveTokenFirebase(requestTokenFirebase);
    }
}
