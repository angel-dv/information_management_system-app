package com.yazaki.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.yazaki.myapplication.common.Constants;
import com.yazaki.myapplication.common.SharedPreferencesManager;
import com.yazaki.myapplication.data.MyViewModel;
import com.yazaki.myapplication.retrofit.request.firebase.RequestTokenFirebase;
import com.yazaki.myapplication.retrofit.request.token.RequestToken;
import com.yazaki.myapplication.ui.LoginActivity;
import com.yazaki.myapplication.ui.controller.SectionsPagerAdapter;
import com.yazaki.myapplication.ui.messages.ContentMessage;
import com.yazaki.myapplication.ui.messages.MessageFragment;
import com.yazaki.myapplication.ui.profile.ProfileFragment;

public class TabsActivity extends AppCompatActivity implements MessageFragment.OnMessageFragmentListener, ProfileFragment.OnProfileFragmentListener {

    private MyViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this).get(MyViewModel.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);
        controller();
        tokenFirebase();
    }

    private void controller() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        toolbar.setTitle(getResources().getString(R.string.title_general_app));

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

//        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
//        params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS | AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL);
    }

    private void tokenFirebase() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            private static final String TAG = "Firebase Token";

            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Log.w(TAG, "getInstanceId failed", task.getException());
                    return;
                }
                // Obtener nuevo Instance ID token
                String tokenFirebase = task.getResult().getToken();
                Log.d(TAG, tokenFirebase);

                if (!tokenFirebase.equals("")) {
                    SharedPreferencesManager.setSomeStringValue(Constants.TOKEN_FIREBASE, tokenFirebase);
                    RequestToken rt = new RequestToken(tokenFirebase);
                    viewModel.setTokenFirebase(rt);
                }
            }
        });
    }

    private void confirmSignOff() {
        final String tokenFirebase = SharedPreferencesManager.getSomeStringValue(Constants.TOKEN_FIREBASE);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(false)
                .setTitle("Cerrando sesión por seguridad")
                .setMessage("Por favor, vuelva a inciar sesión, el token a expirado")

                .setPositiveButton("Cerrar sesión", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        RequestTokenFirebase requestTokenFirebase = new RequestTokenFirebase(tokenFirebase);
                        viewModel.setRemoveTokenFirebase(requestTokenFirebase);

                        SharedPreferencesManager.removeDataValue(Constants.LOGIN_USER_SAP);
                        SharedPreferencesManager.removeDataValue(Constants.LOGIN_USER_PASS);
                        SharedPreferencesManager.removeDataValue(Constants.LOGIN_USER_TOKEN);

                        SharedPreferencesManager.removeDataValue(Constants.PROFILE_USERNAME);
                        SharedPreferencesManager.removeDataValue(Constants.PROFILE_SAP_ID);
                        SharedPreferencesManager.removeDataValue(Constants.PROFILE_AREA);
                        SharedPreferencesManager.removeDataValue(Constants.PROFILE_EMAIL);
                        SharedPreferencesManager.removeDataValue(Constants.PROFILE_PHONE);

                        SharedPreferencesManager.removeDataValue(Constants.REFRESH_TOKEN);
                        SharedPreferencesManager.removeDataValue(Constants.ID_MESSAGE_SELECTED);

                        Intent i = new Intent(TabsActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void setIdMessage(int idMessage) {
        Intent i = new Intent(this, ContentMessage.class);
        i.putExtra(Constants.ID_MESSAGE_TO_CONTENT_MESSAGE, idMessage);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void profile(String profile) {
        Log.d("TAG", profile);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Handler h = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                if (SharedPreferencesManager.getSomeBooleanValue(Constants.REFRESH_TOKEN)) {
                    confirmSignOff();
                }
            }
        };
        h.postDelayed(r, 2000);
    }

}