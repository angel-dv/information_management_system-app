package com.yazaki.myapplication.retrofit.response.messages;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.yazaki.myapplication.common.MyApp;

public class MessageList {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("fecha")
    @Expose
    private String fecha;
    @SerializedName("usuario")
    @Expose
    private Usuario usuario;
    @SerializedName("conf")
    @Expose
    private Integer conf;
    @SerializedName("titulo")
    @Expose
    private String titulo;

    public MessageList(MessageList messageList, int id) {
        if (id == messageList.getId()) {
            Log.d("TAG", messageList.toString());
            this.id = messageList.getId();
            this.fecha = messageList.getFecha();
            this.usuario = (Usuario) messageList.getUsuario();
            this.conf = messageList.getConf();
            this.titulo = messageList.getTitulo();
        } else {
            Log.d("TAG-MessageList", messageList.toString());
        }
    }

    public MessageList(int id, String fecha, Usuario usuario, int conf, String titulo) {
        super();
        this.id = id;
        this.fecha = fecha;
        this.usuario = usuario;
        this.conf = conf;
        this.titulo = titulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Object getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getConf() {
        return conf;
    }

    public void setConf(int conf) {
        this.conf = conf;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

}