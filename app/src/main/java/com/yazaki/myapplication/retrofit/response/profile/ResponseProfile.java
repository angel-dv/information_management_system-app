
package com.yazaki.myapplication.retrofit.response.profile;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseProfile {

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("telefono")
    @Expose
    private String telefono;

    @SerializedName("roles")
    @Expose
    private List<Role> roles = new ArrayList<Role>();

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("sapid")
    @Expose
    private String sapid;

    public ResponseProfile() {
    }

    public ResponseProfile(String username, String email, String telefono, List<Role> roles, int id, String sapid) {
        super();
        this.username = username;
        this.email = email;
        this.telefono = telefono;
        this.roles = roles;
        this.id = id;
        this.sapid = sapid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSapid() {
        return sapid;
    }

    public void setSapid(String sapid) {
        this.sapid = sapid;
    }
}
