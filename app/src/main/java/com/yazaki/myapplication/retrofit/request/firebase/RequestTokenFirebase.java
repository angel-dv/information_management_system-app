package com.yazaki.myapplication.retrofit.request.firebase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestTokenFirebase {

    @SerializedName("token")
    @Expose
    String token;

    public RequestTokenFirebase(String token) {
        this.token = token;
    }
}
