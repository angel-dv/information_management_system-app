package com.yazaki.myapplication.retrofit.response.messages;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.yazaki.myapplication.retrofit.response.profile.Role;

public class Usuario {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("roles")
    @Expose
    private List<Role> roles = new ArrayList<Role>();
    @SerializedName("id")
    @Expose
    private int id;


    public Usuario() {
    }

    public Usuario(String username, String email, String password, List<Role> roles, int id) {
        super();
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}