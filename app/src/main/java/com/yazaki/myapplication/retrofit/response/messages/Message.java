package com.yazaki.myapplication.retrofit.response.messages;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("fecha")
    @Expose
    private String fecha;
    @SerializedName("usuario")
    @Expose
    private String usuario;
    @SerializedName("contenido")
    @Expose
    private String contenido;
    @SerializedName("conf")
    @Expose
    private Integer conf;
    @SerializedName("titulo")
    @Expose
    private String titulo;

    public Message() {
    }

    public Message(int id, String fecha, String usuario, String contenido, int conf, String titulo) {
        super();
        this.id = id;
        this.fecha = fecha;
        this.usuario = usuario;
        this.contenido = contenido;
        this.conf = conf;
        this.titulo = titulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Integer getConf() {
        return conf;
    }

    public void setConf(int conf) {
        this.conf = conf;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

}