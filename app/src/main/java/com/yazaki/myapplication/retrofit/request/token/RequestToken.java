package com.yazaki.myapplication.retrofit.request.token;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestToken {

    @SerializedName("token")
    @Expose
    String token;

    RequestToken(){}

    public RequestToken(String token) {
        super();
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
