package com.yazaki.myapplication.retrofit;

import com.yazaki.myapplication.retrofit.request.firebase.RequestTokenFirebase;
import com.yazaki.myapplication.retrofit.request.token.RequestToken;
import com.yazaki.myapplication.retrofit.response.messages.Message;
import com.yazaki.myapplication.retrofit.response.messages.MessageList;
import com.yazaki.myapplication.retrofit.response.profile.ResponseProfile;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AuthService {

    @GET("usuarios/show")
    Call<ResponseProfile> doProfile();

    @GET("mensajes")
    Call<List<MessageList>> getAllMessage();

    @GET("mensajes/show/{id}")
    Call<Message> getOneMessage(@Path("id") int id);

    @GET("mensajes/confirmar/{id}")
    Call<ResponseBody> setConfirmationMessage(@Path("id") int id);

    @POST("usuarios/token")
    Call<ResponseBody> getAccessTokenFirebase(@Body RequestToken requestToken);

    @POST("signout")
    Call<ResponseBody> getRemoveTokenFirebase(@Body RequestTokenFirebase requestTokenFirebase);
}
