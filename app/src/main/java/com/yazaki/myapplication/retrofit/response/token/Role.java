package com.yazaki.myapplication.retrofit.response.token;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Role {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("authority")
    @Expose
    private String authority;

    public Role() {
    }

    public Role(int id, String authority) {
        super();
        this.id = id;
        this.authority = authority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}
