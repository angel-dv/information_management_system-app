package com.yazaki.myapplication.retrofit;

import android.os.AsyncTask;
import android.util.Log;

import com.yazaki.myapplication.common.Constants;
import com.yazaki.myapplication.common.SharedPreferencesManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {

    private boolean refreshToken = false;

    @Override
    public Response intercept(Chain chain) throws IOException {
        String token = SharedPreferencesManager.getSomeStringValue(Constants.LOGIN_USER_TOKEN);
//
//        Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + token).build();
//        return chain.proceed(request);

        Request original = chain.request();
        Request request = original.newBuilder()
                .header("Authorization", "Bearer " + token)
                .method(original.method(), original.body())
                .build();

        final Response response = chain.proceed(request);
        Log.d("INTERCEPTOR-01", "Code : " + response.code());
        if (response.code() == 403) {
            Log.d("INTERCEPTOR-02", "Ya caduco el token, Code:" + response.code());

            refreshToken = true;
            new refreshTokenAsyncTask().execute(refreshToken);

            return response;
        }

        return response;
    }

    private static class refreshTokenAsyncTask extends AsyncTask<Boolean, Void, Void> {

        @Override
        protected Void doInBackground(Boolean... booleans) {
            Log.d("refreshTokenAsyncTask", booleans.toString());
            SharedPreferencesManager.setSomeBooleanValue(Constants.REFRESH_TOKEN, booleans[0]);
            return null;
        }
    }


}
