package com.yazaki.myapplication.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseConfirmMessage {

    @SerializedName("message")
    @Expose
    private String message;

    ResponseConfirmMessage() {}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
