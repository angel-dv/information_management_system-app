package com.yazaki.myapplication.retrofit;

import com.yazaki.myapplication.retrofit.request.login.RequestLogin;
import com.yazaki.myapplication.retrofit.response.login.ResponseLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiService {

    @POST("login")
    Call<ResponseLogin> doLogin(@Body RequestLogin requestLogin);

}
